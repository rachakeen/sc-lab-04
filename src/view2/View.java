package view2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class View extends JFrame{
	
	private JTextArea resultsArea;
	private JComboBox<Object> comboBox;
	private JPanel centerPanel;
	private JButton acceptBt;
	private JTextField nLoops;
	private JLabel nLoopsLabel;
	private JPanel comboPanel;


	public View() {
		// TODO Auto-generated constructor stub
		createFrame();
		createPanel();
		
	}
	public void createFrame(){
		resultsArea = new JTextArea("Choose Nested loops");
		nLoopsLabel = new JLabel("Enter loop amount");
		nLoops = new JTextField(3);
		comboBox = new JComboBox<>();
		comboBox.addItem("Nested Loop 1");
		comboBox.addItem("Nested Loop 2");
		comboBox.addItem("Nested Loop 3");
		comboBox.addItem("Nested Loop 4");
		comboBox.addItem("Nested Loop 5");
		acceptBt = new JButton("Execute"); 
		nLoops.setText("5");
	}
	public void createPanel(){
		centerPanel = new JPanel();
		centerPanel.setLayout(new GridLayout(1, 1));
		centerPanel.add(resultsArea);
		comboPanel = new JPanel();
		comboPanel.add(nLoopsLabel);
		comboPanel.add(nLoops);
		comboPanel.add(comboBox);
		comboPanel.add(acceptBt);
		centerPanel.add(comboPanel);
		add(centerPanel,BorderLayout.CENTER);
	}
	
	public void setListenerBt(ActionListener list){
		acceptBt.addActionListener(list);
	}
	public String getCombo(){
		return (String) comboBox.getSelectedItem();
	}
	public void setResults(String str){
		resultsArea.setText(str);
	}
	public int getnLoopField(){
		String str = nLoops.getText();
		int n = Integer.parseInt(str);
		return n;
	}

}


