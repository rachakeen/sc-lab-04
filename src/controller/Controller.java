package controller;


import model2.Model;
import view2.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {
	private ActionListener list;
	private View frame;
	private Model loops;
	
	public static void main(String[] args){
		new Controller();
	}
	public Controller(){
		loops = new Model();
		frame = new View();
		frame.setVisible(true);
		frame.setSize(300, 150);
		frame.setLocation(500, 300);
		list = new AddComboListener();
		frame.setListenerBt(list);
	}
	class AddComboListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(frame.getCombo() == "Nested Loop 1"){
				frame.setResults(loops.nestedLoop1(frame.getnLoopField()));
			}
			else if(frame.getCombo() == "Nested Loop 2"){
				frame.setResults(loops.nestedLoop2(frame.getnLoopField()));
			}
			else if(frame.getCombo() == "Nested Loop 3"){
				frame.setResults(loops.nestedLoop3(frame.getnLoopField()));
			}
			else if(frame.getCombo() == "Nested Loop 4"){
				frame.setResults(loops.nestedLoop4(frame.getnLoopField()));
			}
			else if(frame.getCombo() == "Nested Loop 5"){
				frame.setResults(loops.nestedLoop5(frame.getnLoopField()));
			}
		}
		
		
		
	}
}
	
