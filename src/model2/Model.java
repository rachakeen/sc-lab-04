package model2;

public class Model {
	public String nestedLoop1(int n){
		String str = "";
		for(int i=1;i<=n;i++){
			for(int j=1;j<=4;j++){
				str += "*";
			}
			str += "\n";

		}
		return str;
	}
	public String nestedLoop2(int n){
		String str = "";
		for(int i=1;i<=n;i++){
			for(int j=1;j<=3;j++){
				str += "*";
			}
			str += "\n";;
		}
		return str;
	}
	public String nestedLoop3(int n){
		String str = "";
		for(int i=1;i<=n;i++){
			for(int j=1;j<=i;j++){
				str += "*";
			}
			str += "\n";;
		}
		return str;
	}
	public String nestedLoop4(int n){
		String str = "";
		for(int i=1;i<=n;i++){
			for(int j=1;j<=5;j++){
				if(j%2 ==0){
					str += "*";
				}
				else{
					str += " -";
				}
			}
			str += "\n";;
		}
		return str;
	}
	public String nestedLoop5(int n){
		String str = "";
		for(int i=1;i<=n;i++){
			for(int j=1;j<=5;j++){
				if((i+j)%2 ==0){
					str += "*";
				}
				else{
					str += " ";
				}
			}
			str += "\n";;
		}
		return str;
	}

}